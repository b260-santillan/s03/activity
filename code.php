<?php 
	class Person {
		public $firstName;
		public $middleName;
		public $lastName;

		public function __construct ($firstName, $middleName, $lastName) {
			$this->firstName = $firstName;
			$this->middleName = $middleName;
			$this->lastName = $lastName;	
		}


		public function printName() {
			return "Your Full Name is $this->firstName $this->middleName $this->lastName";
		}
		
	}

	class Developer extends Person {
		public function printName() {
			return "Your Name is $this->firstName $this->middleName $this->lastName and you are a developer";
		}
	}

	class Engineer extends Person {
		public function printName() {
			return "Your are and Engineer named $this->firstName $this->middleName $this->lastName";
		}
	}

	$person = new Person ('Senku', 'M.', 'Ishigami');
	$developer = new Developer ('John', 'Finch', 'Smith');
	$engineer = new Engineer ('Harold', 'Myers', 'Reese');

 ?>