<?php require_once './code.php' ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		S03 - Activity
	</title>
</head>
<body>

	<div style="padding: 10px; border: 2px solid black; width: auto; display: inline-block;">
		<h1>Activity</h1>
		<div>
			<h3>Person</h3>
			<?php echo $person->printName() ?>
		</div>
		<div>
			<h3>Developer</h3>
			<?php echo $developer->printName() ?>
		</div>
		<div>
			<h3>Engineer</h3>
			<?php echo $engineer->printName() ?>
		</div>
	</div>

</body>
</html>